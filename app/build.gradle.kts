/*
 * Copyright (c) 2019 Peter Scully <peter@peterscully.name>
 *
 * Licence: CC-BY-4.0, https://creativecommons.org/licenses/by/4.0/
 */

import de.mannodermaus.gradle.plugins.junit5.junitPlatform
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("com.android.application")
    id("kotlin-android")
    id("androidx.navigation.safeargs.kotlin")
//    id("jacoco")
}
apply(plugin ="de.mannodermaus.android-junit5")

tasks.withType(KotlinCompile::class).configureEach {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

//jacoco {
//    toolVersion = "0.8.3"
//    reportsDir = file("$buildDir/reports/jacoco")
//}

android {
    compileSdk = 31
    buildToolsVersion = "31.0.0"
    defaultConfig {
        applicationId = "name.peterscully.learning.navigationlifecycle"
        minSdk = 23
        targetSdk = 31
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        getByName("debug") {
            applicationIdSuffix = ".debug"
            versionNameSuffix = "-debug"
        }
        getByName("release") {
            isMinifyEnabled = false
            isShrinkResources = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }
    buildFeatures {
        viewBinding = true
    }
//    compileOptions {
//        sourceCompatibility = JavaVersion.VERSION_1_8
//        targetCompatibility = JavaVersion.VERSION_1_8
//    }
    testOptions {
        junitPlatform {
            filters {
                includeEngines("spek2")
                excludeEngines("junit-vintage")
            }
//            jacocoOptions {
//                // here goes all jacoco config, for example
//                html.enabled = true
//                xml.enabled = false
//                csv.enabled = false
//                unitTests.all {
////                    testLogging.events = ["passed", "skipped", "failed"]
//                }
//            }
//            testLogging {
//                events("passed", "skipped", "failed")
//            }
//            reports {
//                html.enabled = true
//            }
        }
        unitTests.isIncludeAndroidResources = true
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    // Kotlin
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.5.30")

    // Android
    implementation("androidx.core:core-ktx:1.7.0-alpha02")
    implementation("androidx.activity:activity-ktx:1.4.0-alpha01")
    implementation("androidx.cardview:cardview:1.0.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.0")
    implementation("androidx.fragment:fragment-ktx:1.4.0-alpha08")
    implementation("androidx.lifecycle:lifecycle-extensions:2.2.0")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.4.0-alpha03")
    implementation("androidx.recyclerview:recyclerview:1.2.1")
    implementation("androidx.navigation:navigation-fragment-ktx:2.4.0-alpha08")
    implementation("androidx.navigation:navigation-ui-ktx:2.4.0-alpha08")
    implementation("com.google.android.material:material:1.5.0-alpha03")

    // Timber
    implementation("com.jakewharton.timber:timber:5.0.1")


    testImplementation("org.jetbrains.kotlin:kotlin-reflect:1.5.30")
    // Unit Tests (JUnit5, and support for Spek)
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.0")
    testImplementation("com.willowtreeapps.assertk:assertk-jvm:0.24")

    // Kotlin Unit (Spek) Tests
    testImplementation("org.spekframework.spek2:spek-dsl-jvm:2.0.17")
    testImplementation("org.jetbrains.kotlin:kotlin-test:1.5.30")
    testRuntimeOnly("org.spekframework.spek2:spek-runner-junit5:2.0.17")
    testImplementation("org.mockito:mockito-core:3.12.4")
    testImplementation("com.nhaarman:mockito-kotlin:1.6.0")

    // Instrumentation (Espresso) Tests
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.0-alpha01")
    androidTestImplementation("androidx.test.espresso:espresso-intents:3.5.0-alpha01")
    androidTestRuntimeOnly("androidx.test:runner:1.4.1-alpha01")
    androidTestImplementation("androidx.test:rules:1.4.1-alpha01")
    androidTestImplementation("com.willowtreeapps.assertk:assertk-jvm:0.24")
}
