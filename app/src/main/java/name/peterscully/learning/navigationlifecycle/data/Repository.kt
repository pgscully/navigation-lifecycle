/*
 * Copyright (c) 2019 Peter Scully <peter@peterscully.name>
 *
 * Licence: CC-BY-4.0, https://creativecommons.org/licenses/by/4.0/
 */

package name.peterscully.learning.navigationlifecycle.data

import name.peterscully.learning.navigationlifecycle.data.RepositoryAction.AddEvent
import name.peterscully.learning.navigationlifecycle.data.RepositoryAction.Clear
import java.util.LinkedList

class Repository : Observable<List<LifeCycleEvent>> {

    private val storedEvents: MutableList<LifeCycleEvent> = LinkedList()
    private val observers: MutableList<Observer<List<LifeCycleEvent>>> = LinkedList()

    fun action(action: RepositoryAction) {
        when (action) {
            is Clear -> {
                clear()
            }
            is AddEvent -> {
                add(action.event)
            }
        }
    }

    private fun add(event: LifeCycleEvent) {
        storedEvents.add(event)
        notifyObservers()
    }

    private fun clear() {
        storedEvents.clear()
        notifyObservers()
    }

    private fun notifyObservers() {
        observers.map { it.update(payload = storedEvents) }
    }

    override fun add(observer: Observer<List<LifeCycleEvent>>) {
        observers.add(observer)
    }

    override fun remove(observer: Observer<List<LifeCycleEvent>>) {
        observers.remove(observer)
    }

    companion object {
        val instance = Repository()
    }
}
