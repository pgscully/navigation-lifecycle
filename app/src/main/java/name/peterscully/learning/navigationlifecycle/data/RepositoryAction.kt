/*
 * Copyright (c) 2019 Peter Scully <peter@peterscully.name>
 *
 * Licence: CC-BY-4.0, https://creativecommons.org/licenses/by/4.0/
 */

package name.peterscully.learning.navigationlifecycle.data

sealed class RepositoryAction {
    object Clear : RepositoryAction()
    class AddEvent(val event: LifeCycleEvent) : RepositoryAction()
}
