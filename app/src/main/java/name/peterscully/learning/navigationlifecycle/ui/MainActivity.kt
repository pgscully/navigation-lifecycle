/*
 * Copyright (c) 2019 Peter Scully <peter@peterscully.name>
 *
 * Licence: CC-BY-4.0, https://creativecommons.org/licenses/by/4.0/
 */

package name.peterscully.learning.navigationlifecycle.ui

import android.content.res.Configuration
import android.content.res.Configuration.ORIENTATION_LANDSCAPE
import android.content.res.Configuration.ORIENTATION_PORTRAIT
import android.content.res.Configuration.ORIENTATION_UNDEFINED
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import name.peterscully.learning.navigationlifecycle.R
import name.peterscully.learning.navigationlifecycle.data.LifeCycleEvent
import name.peterscully.learning.navigationlifecycle.data.Repository
import name.peterscully.learning.navigationlifecycle.data.RepositoryAction
import name.peterscully.learning.navigationlifecycle.data.RepositoryAction.AddEvent
import name.peterscully.learning.navigationlifecycle.data.RotationEvent
import name.peterscully.learning.navigationlifecycle.data.SaveRestoreEvent
import timber.log.Timber
import java.util.Calendar

class MainActivity : AppCompatActivity() {

    private var screenColour = 0
    private var saveCount = 0
    private var restoreCount = 0
    private var oldOrientation = ORIENTATION_UNDEFINED

    private val now: Long
        get() = Calendar.getInstance().timeInMillis

    private val repository = Repository.instance

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        navController = findNavController(R.id.main_nav_container)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)

        screenColour = resources.getColor(R.color.mainActivity, null)
        Timber.d("onCreate")
        logCall("onCreate")
    }

    public override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        Timber.d("onPostCreate")
        logCall(methodName = "onPostCreate")
    }

    public override fun onDestroy() {
        Timber.d("onDestroy")
        logCall(methodName = "onDestroy")
        super.onDestroy()
    }

    public override fun onStart() {
        super.onStart()

        Timber.d("onStart")
        logCall("onStart")
    }

    public override fun onRestart() {
        super.onRestart()
        Timber.d("onRestart")
        logCall(methodName = "onRestart")
    }

    public override fun onStop() {
        super.onStop()
        Timber.d("onStop")
        logCall(methodName = "onStop")
    }

    public override fun onResume() {
        super.onResume()
        Timber.d("onResume")
        logCall(methodName = "onResume")
    }

    public override fun onPause() {
        Timber.d("onPause")
        logCall(methodName = "onPause")
        super.onPause()
    }

    public override fun onPostResume() {
        super.onPostResume()
        Timber.d("onPostResume")
        logCall(methodName = "onPostResume")
    }

    public override fun onSaveInstanceState(savedInstanceState: Bundle) {
        saveCount++

        super.onSaveInstanceState(savedInstanceState)
        savedInstanceState.putInt(SAVE_KEY, saveCount)
        savedInstanceState.putInt(RESTORE_KEY, restoreCount)

        Timber.d("onSaveInstanceState")
        logCall(
            methodName = "onSaveInstanceState", saveCount = saveCount,
            restoreCount = restoreCount
        )
    }

    public override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        saveCount = savedInstanceState.getInt(SAVE_KEY, 0)
        restoreCount = savedInstanceState.getInt(RESTORE_KEY, 0)

        restoreCount++

        Timber.d("onRestoreInstanceState")
        logCall(
            methodName = "onRestoreInstanceState", saveCount = saveCount,
            restoreCount = restoreCount
        )
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        Timber.d("onConfigurationChanged")
        if (oldOrientation != newConfig.orientation) {
            logCall(methodName = "onConfigurationChanged", orientation = newConfig.orientation)
        }
        oldOrientation = newConfig.orientation
    }

    override fun onSupportNavigateUp(): Boolean {
        Timber.d("onSupportNavigateUp")
        logCall(methodName = "onSupportNavigateUp")
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Timber.d("onBackPressed")
        logCall(methodName = "onBackPressed")
    }

    override fun onUserLeaveHint() {
        super.onUserLeaveHint()
        Timber.d("onUserLeaveHint")
        logCall(methodName = "onUserLeaveHint")
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        logCall(methodName = "onCreateOptionsMenu")
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    fun onMenuClear(item: MenuItem) {
        repository.action(action = RepositoryAction.Clear)
        logCall(methodName = "Menu->Clear")
    }

    fun onMenuAbout(item: MenuItem) {
        logCall(methodName = "Menu->About")
    }

    private fun logCall(methodName: String) {
        val event = LifeCycleEvent(
            screenName = SCREEN_NAME,
            action = methodName,
            timestamp = now,
            colour = screenColour
        )
        repository.action(action = AddEvent(event = event))
    }

    private fun logCall(methodName: String, orientation: Int) {
        val event = RotationEvent(
            screenName = SCREEN_NAME,
            action = methodName,
            orientation = orientation(orientation),
            timestamp = now,
            colour = screenColour
        )
        repository.action(action = AddEvent(event = event))
    }

    private fun logCall(methodName: String, saveCount: Int, restoreCount: Int) {
        val event = SaveRestoreEvent(
            screenName = SCREEN_NAME,
            action = methodName,
            saveCount = saveCount,
            restoreCount = restoreCount,
            timestamp = now,
            colour = screenColour
        )
        repository.action(action = AddEvent(event = event))
    }

    private fun orientation(orientation: Int): String {
        return when (orientation) {
            ORIENTATION_LANDSCAPE -> "Landscape"
            ORIENTATION_PORTRAIT -> "Portrait"
            else -> "Unknown"
        }
    }

    companion object {
        private const val SCREEN_NAME = "Main Activity"
        private const val SAVE_KEY = "saves"
        private const val RESTORE_KEY = "restores"
    }
}
