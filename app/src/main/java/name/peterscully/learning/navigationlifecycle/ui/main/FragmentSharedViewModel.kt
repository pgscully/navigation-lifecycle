/*
 * Copyright (c) 2019 Peter Scully <peter@peterscully.name>
 *
 * Licence: CC-BY-4.0, https://creativecommons.org/licenses/by/4.0/
 */

package name.peterscully.learning.navigationlifecycle.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import name.peterscully.learning.navigationlifecycle.data.LifeCycleEvent
import name.peterscully.learning.navigationlifecycle.data.Observer
import name.peterscully.learning.navigationlifecycle.data.Repository

class FragmentSharedViewModel : ViewModel(), Observer<List<LifeCycleEvent>> {

    private val viewState = MutableLiveData<List<LifeCycleEvent>>()
    private val repository = Repository.instance

    init {
        repository.add(this)
    }

    override fun onCleared() {
        super.onCleared()
        repository.remove(this)
    }

    fun observableViewState(): LiveData<List<LifeCycleEvent>> {
        return viewState
    }

    override fun update(payload: List<LifeCycleEvent>) {
        viewState.postValue(payload)
    }
}
