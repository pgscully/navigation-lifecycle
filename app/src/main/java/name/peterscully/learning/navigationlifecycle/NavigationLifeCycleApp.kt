/*
 * Copyright (c) 2019 Peter Scully <peter@peterscully.name>
 *
 * Licence: CC-BY-4.0, https://creativecommons.org/licenses/by/4.0/
 */

package name.peterscully.learning.navigationlifecycle

import android.app.Application
import android.content.res.Configuration
import android.content.res.Configuration.ORIENTATION_LANDSCAPE
import android.content.res.Configuration.ORIENTATION_PORTRAIT
import android.content.res.Configuration.ORIENTATION_UNDEFINED
import name.peterscully.learning.navigationlifecycle.data.LifeCycleEvent
import name.peterscully.learning.navigationlifecycle.data.Repository
import name.peterscully.learning.navigationlifecycle.data.RepositoryAction.AddEvent
import name.peterscully.learning.navigationlifecycle.data.RotationEvent
import timber.log.Timber
import timber.log.Timber.DebugTree
import java.util.Calendar


class NavigationLifeCycleApp : Application() {

    private var screenColour = 0
    private var oldOrientation = ORIENTATION_UNDEFINED

    private val now: Long
        get() = Calendar.getInstance().timeInMillis

    private val repository = Repository.instance

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }

        screenColour = resources.getColor(R.color.application, null)

        resources.getStringArray(R.array.intro_text_array).forEach { it -> logCall(it) }

        Timber.d("onCreate")
        logCall("onCreate")
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if (oldOrientation != newConfig.orientation) {
            logCall(methodName = "onConfigurationChanged", orientation = newConfig.orientation)
        }
        oldOrientation = newConfig.orientation
    }

    private fun logCall(methodName: String) {
        val event = LifeCycleEvent(
            screenName = SCREEN_NAME,
            action = methodName,
            timestamp = now,
            colour = screenColour
        )
        repository.action(action = AddEvent(event = event))
    }

    private fun logCall(methodName: String, orientation: Int) {
        val event = RotationEvent(
            screenName = SCREEN_NAME,
            action = methodName,
            orientation = orientation(orientation),
            timestamp = now,
            colour = screenColour
        )
        repository.action(action = AddEvent(event = event))
    }

    private fun orientation(orientation: Int): String {
        return when (orientation) {
            ORIENTATION_LANDSCAPE -> "Landscape"
            ORIENTATION_PORTRAIT -> "Portrait"
            else -> "Unknown"
        }
    }

    companion object {
        private const val SCREEN_NAME = "Application"
    }
}
