/*
 * Copyright (c) 2019 Peter Scully <peter@peterscully.name>
 *
 * Licence: CC-BY-4.0, https://creativecommons.org/licenses/by/4.0/
 */

package name.peterscully.learning.navigationlifecycle.ui.main

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.content.res.Configuration.ORIENTATION_LANDSCAPE
import android.content.res.Configuration.ORIENTATION_PORTRAIT
import android.content.res.Configuration.ORIENTATION_UNDEFINED
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import name.peterscully.learning.navigationlifecycle.R
import name.peterscully.learning.navigationlifecycle.data.LifeCycleEvent
import name.peterscully.learning.navigationlifecycle.data.Repository
import name.peterscully.learning.navigationlifecycle.data.RepositoryAction.AddEvent
import name.peterscully.learning.navigationlifecycle.data.RotationEvent
import name.peterscully.learning.navigationlifecycle.data.SaveRestoreEvent
import name.peterscully.learning.navigationlifecycle.databinding.FragmentOneBinding
import timber.log.Timber
import java.util.Calendar

class FragmentOne : Fragment() {

    private lateinit var viewModel: FragmentSharedViewModel
    private lateinit var adapter: LifeCycleEventAdapter

    private var _binding: FragmentOneBinding? = null
    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    private val now: Long
        get() = Calendar.getInstance().timeInMillis

    private val repository = Repository.instance

    private var screenColour = 0
    private var saveCount = 0
    private var restoreCount = 0
    private var oldOrientation = ORIENTATION_UNDEFINED

    override fun onAttach(context: Context) {
        super.onAttach(context)
        screenColour = activity?.resources?.getColor(R.color.fragmentOne, null)!!
        Timber.d("onAttach")
        logCall(methodName = "onAttach")
    }

    override fun onDetach() {
        super.onDetach()
        Timber.d("onDetach")
        logCall(methodName = "onDetach")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("onCreate")
        logCall(methodName = "onCreate")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentOneBinding.inflate(inflater, container, false)
        val view = binding.root
        Timber.d("onCreateView, view = $view")
        logCall(methodName = "onCreateView")
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.d("onViewCreated")
        logCall(methodName = "onViewCreated")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        Timber.d("onDestroyView")
        logCall(methodName = "onDestroyView")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = activity?.run {
            ViewModelProviders.of(this).get(FragmentSharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        val args = FragmentOneArgs.fromBundle(requireArguments())
        setupRecyclerView(position = args.position)
        Timber.d("onActivityCreated")
        logCall(methodName = "onActivityCreated")

        viewModel.observableViewState().observe(viewLifecycleOwner, Observer { it -> render(it) })
        _binding?.fragOneButtonOne?.setOnClickListener { onButtonOneClick() }
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("onDestroy")
        logCall(methodName = "onDestroy")
    }

    override fun onStart() {
        super.onStart()
        Timber.d("onStart")
        logCall(methodName = "onStart")
    }

    override fun onStop() {
        super.onStop()
        Timber.d("onStop")
        logCall(methodName = "onStop")
    }

    override fun onPause() {
        super.onPause()
        Timber.d("onPause")
        logCall(methodName = "onPause")
    }

    override fun onResume() {
        super.onResume()
        Timber.d("onResume")
        logCall(methodName = "onResume")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        saveCount++
        outState.apply {
            putInt(SAVE_KEY, saveCount)
            putInt(RESTORE_KEY, restoreCount)
        }
        Timber.d("onSaveInstanceState")
        logCall(
            methodName = "onSaveInstanceState",
            saveCount = saveCount,
            restoreCount = restoreCount
        )
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        savedInstanceState?.apply {
            saveCount = getInt(SAVE_KEY)
            restoreCount = getInt(RESTORE_KEY)
        }
        restoreCount++
        Timber.d("onViewStateRestored")
        logCall(
            methodName = "onViewStateRestored",
            saveCount = saveCount,
            restoreCount = restoreCount
        )
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        Timber.d("onConfigurationChanged")
        if (oldOrientation != newConfig.orientation) {
            logCall(methodName = "onConfigurationChanged", orientation = newConfig.orientation)
        }
        oldOrientation = newConfig.orientation
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Timber.d("onActivityResult")
        logCall(methodName = "onActivityResult")
    }

    private fun setupRecyclerView(position: Int) {
        Timber.d("setupRecyclerView")
        _binding?.fragOneRecyclerView?.layoutManager = LinearLayoutManager(context).apply {
            this.orientation = RecyclerView.VERTICAL
        }
        this.adapter = LifeCycleEventAdapter()
        _binding?.fragOneRecyclerView?.adapter = this.adapter
        _binding?.fragOneRecyclerView?.scrollToPosition(position)
    }

    private fun render(events: List<LifeCycleEvent>) {
        Timber.d("render, count = ${events.size}")
        adapter.submitList(events)
        adapter.notifyDataSetChanged()
    }

    private fun onButtonOneClick() {
        logCall(methodName = "Button.onClick -> to Fragment Two")
        val directions = FragmentOneDirections.actionFragmentOneToFragTwoThreeNav(
            position = (_binding?.fragOneRecyclerView?.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
        )
        Navigation.findNavController(_binding!!.root).navigate(directions)
    }

    private fun logCall(methodName: String) {
        val event = LifeCycleEvent(
            screenName = SCREEN_NAME,
            action = methodName,
            timestamp = now,
            colour = screenColour
        )
        repository.action(action = AddEvent(event = event))
    }

    private fun logCall(methodName: String, orientation: Int) {
        val event = RotationEvent(
            screenName = SCREEN_NAME,
            action = methodName,
            orientation = orientation(orientation),
            timestamp = now,
            colour = screenColour
        )
        repository.action(action = AddEvent(event = event))
    }

    private fun logCall(methodName: String, saveCount: Int, restoreCount: Int) {
        val event = SaveRestoreEvent(
            screenName = SCREEN_NAME,
            action = methodName,
            saveCount = saveCount,
            restoreCount = restoreCount,
            timestamp = now,
            colour = screenColour
        )
        repository.action(action = AddEvent(event = event))
    }

    private fun orientation(orientation: Int): String {
        return when (orientation) {
            ORIENTATION_LANDSCAPE -> "Landscape"
            ORIENTATION_PORTRAIT -> "Portrait"
            else -> "Unknown"
        }
    }

    companion object {
        private const val SCREEN_NAME = "Fragment One"
        private const val SAVE_KEY = "saves"
        private const val RESTORE_KEY = "restores"
    }
}
