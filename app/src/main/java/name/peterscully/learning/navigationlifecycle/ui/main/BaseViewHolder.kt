/*
 * Copyright (c) 2019 Peter Scully <peter@peterscully.name>
 *
 * Licence: CC-BY-4.0, https://creativecommons.org/licenses/by/4.0/
 */

package name.peterscully.learning.navigationlifecycle.ui.main

import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * The Base view holder.  Exists to be extended by other view holders.
 */
open class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
