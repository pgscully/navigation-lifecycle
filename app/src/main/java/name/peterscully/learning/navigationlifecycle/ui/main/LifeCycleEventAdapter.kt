/*
 * Copyright (c) 2019 Peter Scully <peter@peterscully.name>
 *
 * Licence: CC-BY-4.0, https://creativecommons.org/licenses/by/4.0/
 */

package name.peterscully.learning.navigationlifecycle.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import name.peterscully.learning.navigationlifecycle.R
import name.peterscully.learning.navigationlifecycle.data.LifeCycleEvent
import name.peterscully.learning.navigationlifecycle.data.RotationEvent
import name.peterscully.learning.navigationlifecycle.data.SaveRestoreEvent
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class LifeCycleEventAdapter : ListAdapter<LifeCycleEvent, BaseViewHolder>(
    object : DiffUtil.ItemCallback<LifeCycleEvent>() {
        override fun areItemsTheSame(oldItem: LifeCycleEvent, newItem: LifeCycleEvent): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: LifeCycleEvent, newItem: LifeCycleEvent): Boolean {
            return oldItem == newItem
        }
    }
) {

    private val dateFormatter = SimpleDateFormat("HH:mm:ss.SSS", Locale.getDefault())

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            LIFECYCLE_EVENT -> {
                val itemView = LayoutInflater.from(viewGroup.context)
                    .inflate(R.layout.lifecycle_card_view, viewGroup, false)
                LifeCycleViewHolder(itemView = itemView)
            }
            SAVE_RESTORE_EVENT -> {
                val itemView = LayoutInflater.from(viewGroup.context)
                    .inflate(R.layout.save_restore_card_view, viewGroup, false)
                SaveRestoreViewHolder(itemView = itemView)
            }
            ROTATION_EVENT -> {
                val itemView = LayoutInflater.from(viewGroup.context)
                    .inflate(R.layout.rotation_card_view, viewGroup, false)
                RotationViewHolder(itemView = itemView)
            }
            else -> throw IllegalArgumentException("Unknown view type: $viewType")
        }
    }

    override fun onBindViewHolder(viewHolder: BaseViewHolder, position: Int) {
        when (viewHolder.itemViewType) {
            LIFECYCLE_EVENT -> {
                val event = getItem(position)
                val vh = viewHolder as LifeCycleViewHolder
                vh.activityText.text = event.screenName
                vh.eventText.text = event.action
                vh.dateText.text = dateFormatter.format(Date(event.timestamp))
                vh.itemView.setBackgroundColor(event.colour)
            }
            SAVE_RESTORE_EVENT -> {
                val event = getItem(position) as SaveRestoreEvent
                val vh = viewHolder as SaveRestoreViewHolder
                vh.activityText.text = event.screenName
                vh.eventText.text = event.action
                vh.dateText.text = dateFormatter.format(Date(event.timestamp))
                vh.saveCount.text = String.format(Locale.getDefault(), "%d", event.saveCount)
                vh.restoreCount.text = String.format(Locale.getDefault(), "%d", event.restoreCount)
                vh.itemView.setBackgroundColor(event.colour)
            }
            ROTATION_EVENT -> {
                val event = getItem(position) as RotationEvent
                val vh = viewHolder as RotationViewHolder
                vh.activityText.text = event.screenName
                vh.eventText.text = event.action
                vh.dateText.text = dateFormatter.format(Date(event.timestamp))
                vh.orientationText.text = event.orientation
                vh.itemView.setBackgroundColor(event.colour)
            }
            else -> {
                throw IllegalArgumentException("Unknown view type: ${viewHolder.itemViewType}")
            }
        }
    }

    override fun getItemViewType(position: Int): Int = when {
        getItem(position) is SaveRestoreEvent -> SAVE_RESTORE_EVENT
        getItem(position) is RotationEvent -> ROTATION_EVENT
        else -> LIFECYCLE_EVENT
    }

    companion object {
        private const val LIFECYCLE_EVENT = 0
        private const val SAVE_RESTORE_EVENT = 1
        private const val ROTATION_EVENT = 2
    }
}
