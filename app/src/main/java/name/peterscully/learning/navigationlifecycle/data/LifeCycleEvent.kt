/*
 * Copyright (c) 2019 Peter Scully <peter@peterscully.name>
 *
 * Licence: CC-BY-4.0, https://creativecommons.org/licenses/by/4.0/
 */

package name.peterscully.learning.navigationlifecycle.data

open class LifeCycleEvent(
    val screenName: String,
    val action: String,
    val timestamp: Long,
    val colour: Int
)
