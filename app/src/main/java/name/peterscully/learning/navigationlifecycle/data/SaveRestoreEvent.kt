/*
 * Copyright (c) 2019 Peter Scully <peter@peterscully.name>
 *
 * Licence: CC-BY-4.0, https://creativecommons.org/licenses/by/4.0/
 */

package name.peterscully.learning.navigationlifecycle.data

class SaveRestoreEvent(
    screenName: String,
    action: String,
    timestamp: Long,
    val saveCount: Int,
    val restoreCount: Int,
    colour: Int
) : LifeCycleEvent(
    screenName = screenName,
    action = action,
    timestamp = timestamp,
    colour = colour
)
