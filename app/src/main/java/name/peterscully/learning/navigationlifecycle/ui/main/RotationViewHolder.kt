/*
 * Copyright (c) 2019 Peter Scully <peter@peterscully.name>
 *
 * Licence: CC-BY-4.0, https://creativecommons.org/licenses/by/4.0/
 */

package name.peterscully.learning.navigationlifecycle.ui.main

import android.view.View
import android.widget.TextView
import name.peterscully.learning.navigationlifecycle.R

class RotationViewHolder(itemView: View) : BaseViewHolder(itemView) {
    var activityText: TextView = itemView.findViewById(R.id.rotationActivityText)
    var eventText: TextView = itemView.findViewById(R.id.rotationEventText)
    var dateText: TextView = itemView.findViewById(R.id.rotationDateText)
    var orientationText: TextView = itemView.findViewById(R.id.rotationOrientation)
}
