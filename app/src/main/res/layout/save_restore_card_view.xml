<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Copyright (c) 2019 Peter Scully <peter@peterscully.name>
  ~
  ~ Licence: CC-BY-4.0, https://creativecommons.org/licenses/by/4.0/
  -->

<androidx.cardview.widget.CardView
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:card_view="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:id="@+id/save_restore_card_view"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginBottom="@dimen/margin_bottom"
    android:layout_marginEnd="@dimen/margin_end"
    android:layout_marginStart="@dimen/margin_start"
    android:layout_marginTop="@dimen/margin_top"
    card_view:cardCornerRadius="2dp"
    >

  <androidx.constraintlayout.widget.ConstraintLayout
      android:layout_width="match_parent"
      android:layout_height="wrap_content"
      android:layout_marginBottom="@dimen/margin_bottom"
      android:layout_marginEnd="@dimen/margin_end"
      android:layout_marginStart="@dimen/margin_start"
      android:layout_marginTop="@dimen/margin_top"
      >

    <androidx.constraintlayout.widget.Guideline
        android:id="@+id/save_restore_guideline"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:orientation="vertical"
        app:layout_constraintGuide_percent="0.25"
        tools:layout_editor_absoluteX="148dp"
        tools:layout_editor_absoluteY="77dp"
        />

    <TextView
        android:id="@+id/saveRestoreActivityText"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:gravity="center_vertical|center_horizontal"
        android:lines="3"
        android:padding="@dimen/text_view_padding"
        android:textAlignment="center"
        android:textColor="@android:color/black"
        app:autoSizeTextType="uniform"
        app:autoSizePresetSizes="@array/autosize_text_sizes"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintLeft_toLeftOf="parent"
        app:layout_constraintRight_toLeftOf="@+id/save_restore_guideline"
        app:layout_constraintTop_toTopOf="parent"
        tools:text="Activity"
        />

    <TextView
        android:id="@+id/saveRestoreEventText"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:layout_marginBottom="@dimen/margin_bottom_internal"
        android:layout_marginEnd="@dimen/margin_end"
        android:layout_marginStart="@dimen/margin_start"
        android:gravity="start"
        android:padding="@dimen/text_view_padding"
        android:textAlignment="viewStart"
        android:textColor="@android:color/black"
        app:autoSizeTextType="uniform"
        app:autoSizePresetSizes="@array/autosize_text_sizes"
        app:layout_constraintBottom_toTopOf="@+id/saveRestoreDateText"
        app:layout_constraintHorizontal_chainStyle="spread_inside"
        app:layout_constraintLeft_toLeftOf="@+id/save_restore_guideline"
        app:layout_constraintRight_toLeftOf="@+id/saveRestoreSaveText"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_chainStyle="spread_inside"
        tools:text="onCreate()"
        />

    <TextView
        android:id="@+id/saveRestoreDateText"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:layout_marginStart="@dimen/margin_start"
        android:layout_marginTop="@dimen/margin_top_internal"
        android:gravity="start"
        android:padding="@dimen/text_view_padding"
        android:textAlignment="viewStart"
        android:textColor="@android:color/black"
        app:autoSizeTextType="uniform"
        app:autoSizePresetSizes="@array/autosize_text_sizes"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintLeft_toLeftOf="@+id/save_restore_guideline"
        app:layout_constraintTop_toBottomOf="@+id/saveRestoreEventText"
        tools:text="Date"
        />

    <TextView
        android:id="@+id/saveRestoreSaveText"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginEnd="@dimen/margin_end"
        android:layout_marginStart="@dimen/margin_start"
        android:gravity="end"
        android:padding="@dimen/text_view_padding"
        android:text="@string/save_count"
        android:textAlignment="viewEnd"
        android:textColor="@android:color/black"
        app:autoSizeTextType="uniform"
        app:autoSizePresetSizes="@array/autosize_text_sizes"
        app:layout_constraintBaseline_toBaselineOf="@+id/saveRestoreSaveCount"
        app:layout_constraintBottom_toTopOf="@+id/saveRestoreRestoreText"
        app:layout_constraintLeft_toRightOf="@+id/saveRestoreEventText"
        app:layout_constraintRight_toLeftOf="@+id/saveRestoreSaveCount"
        app:layout_constraintVertical_chainStyle="spread_inside"
        />
    <TextView
        android:id="@+id/saveRestoreSaveCount"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:gravity="end"
        android:padding="@dimen/text_view_padding"
        android:textAlignment="viewEnd"
        android:textColor="@android:color/black"
        app:autoSizeTextType="uniform"
        app:autoSizePresetSizes="@array/autosize_text_sizes"
        app:layout_constraintBottom_toTopOf="@+id/saveRestoreRestoreCount"
        app:layout_constraintRight_toRightOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_chainStyle="spread_inside"
        tools:text="0"
        />

    <TextView
        android:id="@+id/saveRestoreRestoreText"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginEnd="@dimen/margin_end"
        android:gravity="end"
        android:padding="@dimen/text_view_padding"
        android:text="@string/restore_count"
        android:textAlignment="viewEnd"
        android:textColor="@android:color/black"
        app:autoSizeTextType="uniform"
        app:autoSizePresetSizes="@array/autosize_text_sizes"
        app:layout_constraintBaseline_toBaselineOf="@+id/saveRestoreRestoreCount"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintRight_toLeftOf="@+id/saveRestoreRestoreCount"
        app:layout_constraintTop_toBottomOf="@+id/saveRestoreSaveText"
        />
    <TextView
        android:id="@+id/saveRestoreRestoreCount"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:layout_marginTop="@dimen/margin_top_internal"
        android:gravity="end"
        android:padding="@dimen/text_view_padding"
        android:textAlignment="viewEnd"
        android:textColor="@android:color/black"
        app:autoSizeTextType="uniform"
        app:autoSizePresetSizes="@array/autosize_text_sizes"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintRight_toRightOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/saveRestoreSaveCount"
        tools:text="0"
        />
  </androidx.constraintlayout.widget.ConstraintLayout>
</androidx.cardview.widget.CardView>
