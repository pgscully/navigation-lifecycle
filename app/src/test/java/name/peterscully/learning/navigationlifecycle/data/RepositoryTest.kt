/*
 * Copyright (c) 2019 Peter Scully <peter@peterscully.name>
 *
 * Licence: CC-BY-4.0, https://creativecommons.org/licenses/by/4.0/
 */

package name.peterscully.learning.navigationlifecycle.data

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import com.nhaarman.mockito_kotlin.verifyZeroInteractions
import name.peterscully.learning.navigationlifecycle.data.RepositoryAction.AddEvent
import name.peterscully.learning.navigationlifecycle.data.RepositoryAction.Clear
import org.mockito.Mockito
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.gherkin.Feature

object RepositoryTest : Spek({

    Feature("Repository Functionality") {

        Scenario("Adding an Event") {
            val repository1 = Repository()
            val mockObserver1: Observer<List<LifeCycleEvent>> = mock()
            val event1 = LifeCycleEvent(
                screenName = "screen 1",
                action = "action 1",
                timestamp = 1L,
                colour = 1
            )
            val action1 = AddEvent(event = event1)

            repository1.action(action = Clear)
            repository1.add(observer = mockObserver1)

            When("Adding an event") {
                repository1.action(action = action1)
            }

            Then("It should be stored in the Repository") {
                verify(mockObserver1).update(listOf(event1))
                verifyNoMoreInteractions(mockObserver1)
            }
        }

        Scenario("Adding two Events") {
            val repository2 = Repository()
            val mockObserver2: Observer<List<LifeCycleEvent>> = mock()
            val event2 = LifeCycleEvent(
                screenName = "screen 2",
                action = "action 2",
                timestamp = 2L,
                colour = 2
            )
            val action2 = AddEvent(event2)
            val event3 = LifeCycleEvent(
                screenName = "screen 3",
                action = "action 3",
                timestamp = 3L,
                colour = 3
            )
            val action3 = AddEvent(event3)

            repository2.action(Clear)
            repository2.add(observer = mockObserver2)

            When("Adding two events") {
                repository2.action(action = action2)
                repository2.action(action = action3)
            }

            Then("They should be stored in the Repository") {
                // FIXME: This should be:
                // inOrder.verify(mockObserver2).update(listOf(event2))
                // inOrder.verify(mockObserver2).update(listOf(event2, event3))
                val inOrder = Mockito.inOrder(mockObserver2)
                inOrder.verify(mockObserver2, times(2)).update(listOf(event2, event3))
                verifyNoMoreInteractions(mockObserver2)
            }
        }
    }

    Feature("Repository Observable Interface") {

        Scenario("Adding an observer") {
            val repository3 = Repository()
            val mockObserver3: Observer<List<LifeCycleEvent>> = mock()

            When("Adding the observer") {
                repository3.action(Clear)
                repository3.add(observer = mockObserver3)
            }

            Then("It should receive update events") {
                val event4 = LifeCycleEvent(
                    screenName = "screen 4",
                    action = "action 4",
                    timestamp = 4L,
                    colour = 4
                )
                val action4 = AddEvent(event = event4)
                repository3.action(action = action4)

                verify(mockObserver3).update(listOf(event4))
            }
        }

        Scenario("Removing an observer when it was not added") {
            val repository4 = Repository()
            val mockObserver4: Observer<List<LifeCycleEvent>> = mock()

            When("Removing the observer") {
                repository4.action(Clear)
                repository4.remove(observer = mockObserver4)
            }

            Then("It should not throw") {
                assert(true)
            }
        }

        Scenario("Removing an observer") {
            val repository5 = Repository()
            val mockObserver5: Observer<List<LifeCycleEvent>> = mock()

            When("Removing the observer") {
                repository5.action(Clear)
                repository5.add(observer = mockObserver5)
                repository5.remove(observer = mockObserver5)
            }

            Then("It should not receive update events") {
                val event5 = LifeCycleEvent(
                    screenName = "screen 5",
                    action = "action 5",
                    timestamp = 5L,
                    colour = 5
                )
                val action5 = AddEvent(event = event5)
                repository5.action(action = action5)

                verifyZeroInteractions(mockObserver5)
            }
        }
    }
})
