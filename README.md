# Navigation Lifecycle

A learning app to put together the new navigation and other architecture components and get a feel
for how they work.

This is a single activity app, with four fragments, using the [Navigation Architecture
Component](https://developer.android.com/topic/libraries/architecture/navigation/) to navigate
between components.  It records and displays Activity and Fragment lifecycle events.

# Activity and Fragment Lifecycles

The app logs Activity and Lifecycle events, and displays them in the order received.

# Navigation Architecture Component

The app uses the Navigation Architecture Component to navigate between fragments.  Fragments Two
and Three are in their own sub-graph:

F1 -> (F2 -> F3) -> F4

Only one of fragments Two and Three should appear when navigating back to fragment One:

F1 -> F2 -> F3 -> F4 -> F3 -> F1

or

F1 -> F2 -> F3 -> F2 -> F4 -> F2 -> F1
